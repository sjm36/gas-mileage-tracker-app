//
//  Fillup.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import Foundation

class Fillup {
    var carId: Int32                              // Foreign key from Car
    var fillupOdometer: Int32
    var pricePerGallon: Double
    var gallonsFilled: Double
    var totalSpent: Double
    var dateFillup: Int32                         // Stored in database as Unix time
    var filled: Bool                            // Car was filled up
    var fillupID: Int32
    
    init(carIdFK: Int32) {
        carId = carIdFK
        fillupOdometer = 0
        pricePerGallon = 0
        gallonsFilled = 0
        totalSpent = 0
        dateFillup = 0
        fillupID = 0
        filled = false
    }
    
    init(carIDFK: Int32, carFillupOdo: Int32, fuelPricePerGallon: Double, fuelGallonsFilled: Double, fuelTotalSpent: Double, dateOfFillup: Int32, carFilled: Bool, cFillupID: Int32) {
        carId = carIDFK
        fillupOdometer = carFillupOdo
        pricePerGallon = fuelPricePerGallon
        gallonsFilled = fuelGallonsFilled
        totalSpent = fuelTotalSpent
        dateFillup = dateOfFillup
        filled = carFilled
        fillupID = cFillupID
    }

}