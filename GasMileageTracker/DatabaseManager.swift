//
//  DatabaseManager.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
// 
//  Main wrapper class for the SQLite database
//

import Foundation


class DatabaseManager {
    
    var databasePath: String?
    private var lastError: String {
        get {
            let err = self.lastError
            self.lastError = ""
            return err
        }
        
        set (err) {
            // Possible formatting of error
        }
    }
    
    var lastInsertId: Int?
    
    var queryDatabase: FMDatabase?
    
    init(dbaseFile: String) {
        let filemgr = NSFileManager.defaultManager()                    // Start drilling down the directory structure to get to the Database file
        let dirPaths =
        NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        
        let docsDir = dirPaths[0] as String
        
        databasePath = docsDir.stringByAppendingPathComponent(          // The actual database full path
            dbaseFile)
        
        if !filemgr.fileExistsAtPath(databasePath!) {                    // If the database path does not exist, the database does not exist
            
            let database = openDatabase()

            
            if database.open() {
                
                // fillup id key is the fillup id, carid is the link between the car table id and fillup table
                let car_create_sql = "CREATE TABLE IF NOT EXISTS CAR (ID INTEGER PRIMARY KEY AUTOINCREMENT, CARNAME TEXT NOT NULL, INITIALODOMETER INTEGER NOT NULL)"
                let fillup_create_sql = "CREATE TABLE IF NOT EXISTS FILLUP (ID INTEGER PRIMARY KEY AUTOINCREMENT, FILLUPODOMETER INTEGER NOT NULL, PPG REAL NOT NULL, TOTALPRICE REAL NOT NULL, GALLONSFILLED REAL NOT NULL, DATEFILLED INTEGER NOT NULL, FILLED INTEGER NOT NULL, CARID INTEGER NOT NULL)"
                
                if !executeRawStatements(car_create_sql) {
                    setLastError(database.lastErrorMessage())
                }
                
                if !executeRawStatements(fillup_create_sql) {
                    setLastError(database.lastErrorMessage())
                }
                
                closeDatabase(database)
            } else {
                setLastError(database.lastErrorMessage())
            }
        
        }
    }
    
    /// This method executes a query. The database path must have been intialized to use this method.
    ///
    ///
    /// :param: query The actual query you want to execute
    /// :returns: An FMResultSet containing the results of the query.
    func executeRawQuery(query: NSString) -> FMResultSet {
        let dbase = openDatabase()
        queryDatabase = dbase
        var results: FMResultSet
        if !dbase.open() {
            setLastError(dbase.lastErrorMessage())
            return FMResultSet()
        }
        
        // Database is open. Execute the query
        
        let querySQL = query
        results = dbase.executeQuery(querySQL, withArgumentsInArray: nil)
        
        return results
    }
    
    /// This method executes a statement. The database path must have been intialized to use this method.
    ///
    ///
    /// :param: stmnt The actual statement you want to execute
    /// :returns: The success value of the statement
    func executeRawStatements(stmnt: NSString) -> Bool {
        let dbase = openDatabase()
        
        if !dbase.open() {
            setLastError(dbase.lastErrorMessage())
            return false
        }
        
        // Database is open
        // Execute statement. If there's an error, return false
        let statement = stmnt
        if !dbase.executeStatements(statement) {
            return false
        }
        
        lastInsertId = dbase.lastInsertRowId().description.toInt()
        closeDatabase(dbase)
        
        
        return true

    }
    
    /// This method runs an update statement against the database. The database path must have been intialized to use this method.
    ///
    ///
    /// :param: stmnt The actual statement you want to execute
    /// :returns: The success value of the statement
    func executeRawUpdate(stmnt: NSString) -> Bool {
        let dbase = openDatabase()
        
        if !dbase.open() {
            setLastError(dbase.lastErrorMessage())
            return false
        }
        
        // Database is open
        // Execute update. If there's an error, return false
        let statement = stmnt
        if !dbase.executeUpdate(stmnt, withArgumentsInArray: nil) {
            return false
        }
        
        
        closeDatabase(dbase)
        
        return true
    }
    
    func getLastError() -> NSString {
        return lastError
    }
    
    private func setLastError(err: NSString) {
        lastError = err
        println(lastError)
    }
    
    
    private func openDatabase() -> FMDatabase {
        var dbase = FMDatabase(path: databasePath)
        return dbase
    }
    
    private func closeDatabase(close: FMDatabase) {
        close.close()
    }
    
}

extension Int {
    
    func toBool () ->Bool? {
        
        switch self {
            
        case 0:
            
            return false
            
        case 1:
            
            return true
            
        default:
            
            return nil
            
        }
        
    }
    
}
