//
//  CarStore.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import Foundation

class CarStore {
    class var sharedInstance: CarStore {
        struct Static {
            static let instance = CarStore()
        }
        return Static.instance
    }
    
    var cars: [Car] = []
    
    func add(car: Car) {
        cars.append(car)
    }
    
    func replace(car: Car, atIndex index: Int) {
        cars[index] = car
    }
    
    func get(index: Int) -> Car {
        return cars[index]
    }
    
    func removeCarAtIndex(index: Int) {
        cars.removeAtIndex(index)
    }
    
    func count() -> Int {
        return cars.count
    }
    
    func getFillupStore(index: Int) -> FillupStore { 
        return cars[index].getFillupStore()
    }
}
