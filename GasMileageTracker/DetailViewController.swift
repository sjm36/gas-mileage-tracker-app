//
//  DetailViewController.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/1/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import UIKit
import Foundation


class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AddEditViewControllerDelegate {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var fillupList: UITableView!
    var initialFillupMileage: Int32!                // Initial fillup amount from MasterViewController
    var currencySymbol = ""                         // The default currency symbol
    
    private var selectedIndex = 0                   // The holder for the selected index of the car itself

    var detailItem: FillupStore? {                  // The list of fillups from the selected car fillupstore
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    var selectedCarId: Int32?                       // Will be set with the car index of the selected car from MasterView
    
    

    func configureView() {
        // We are setting the currency symbol here
        if let currencyCode = NSLocale.currentLocale().objectForKey(NSLocaleCurrencySymbol) as? String {
            currencySymbol = currencyCode
        }
        
        var fillupListArray: [Int32] = []
        if let ifm = initialFillupMileage {
            fillupListArray.append(ifm)
        } else { return }
        if let totalLength = detailItem?.count() {
            if totalLength != 0 {
                for index in 0...totalLength-1 {
                    if let fillupOdometer = detailItem?.get(index).fillupOdometer {
                        fillupListArray.append(fillupOdometer)
                    }
                }
            }
        }
        
        if let detail: FillupStore = self.detailItem {
            if let label = self.detailDescriptionLabel {
                /// This is where the overall Gas Mileage will be calculated
                
            }
        } else {
            //var segue: UIStoryboardSegue = UIStoryboardSegue(identifier: "ipadPopover", source: self, destination: IpadPopoverController())
            //self.performSegueWithIdentifier("ipadSegue", sender: self) // Perform a segue to the next view controller to prevent an error on ipad
            // icontrol = IpadPopoverController() as UIViewController
            //self.navigationController?.pushViewController(icontrol, animated: true)
        
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// CUSTOM CELL REGISTRATION
        var nib = UINib(nibName: "FillupCell", bundle: nil)                     // Load custom cell from nib
        fillupList.registerNib(nib, forCellReuseIdentifier: "cell")             // Register cell
        /// END CUSTOM CELL REGISTRATION
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let detail: FillupStore = self.detailItem {
            
            return detailItem!.count()
        }
        return 0
    }
    

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        println("In cellForRowAtIndexPath")
        var cell: FillupTableCell = self.fillupList.dequeueReusableCellWithIdentifier("cell") as FillupTableCell
        
        var fillup = detailItem?.get(indexPath.row)

        // Where unix time is converted to a standard date
        if let dateAsInt = fillup?.dateFillup {
            let dateAsDouble = Double(dateAsInt)                                    // Convert date integer to double to be used in NSTimeInterval
            let fmtTime : NSTimeInterval = dateAsDouble as NSTimeInterval           // Get a date from the object from the database
            let finalDate = NSDate(timeIntervalSince1970: fmtTime)                  // Get the time interval since 1970 for use with the unix time
            // Split into the individual date componenents
            var dc: NSDateComponents = NSCalendar.currentCalendar().components(NSCalendarUnit.DayCalendarUnit | NSCalendarUnit.MonthCalendarUnit | NSCalendarUnit.YearCalendarUnit, fromDate: finalDate)
            cell.dateLabel.text = dc.month.description + "/" + dc.day.description + "/" + dc.year.description
        }
        
        var totalString: String = String(format: "%.2f", (fillup?.totalSpent)!)             // Format total spent string
        var ppgString: String = String(format: "%.3f", (fillup?.pricePerGallon)!)           // Format price per gallon string
        var fillupArrays: [Int32] = []
        var fillupTotalArrays: [Double] = []
        if indexPath.row == 0 {
            fillupArrays.append(initialFillupMileage)               // This is the first cell. Compare initial fillup to the current fillup
            if let fillupOdometer = fillup?.fillupOdometer {
                fillupArrays.append(fillupOdometer)
            }
            if let fillupTotal = fillup?.gallonsFilled {
                fillupTotalArrays.append(fillupTotal)
            }
        } else {                                                    // This is another cell. There is a fillup before this one so we can compare.
            if let fillupOdometer = detailItem?.get(indexPath.row - 1){
                fillupArrays.append(fillupOdometer.fillupOdometer)
            }
            if let currentFillup = fillup? {
                fillupArrays.append(currentFillup.fillupOdometer)
                fillupTotalArrays.append(currentFillup.gallonsFilled)   // Only need to know the fillup amount of the current fillup
            }
        }
        cell.mpgLabel.text = calculateGasMileage(fillupArrays, listOfFillupAmounts: fillupTotalArrays)
        
        cell.totalLabel.text = "Total: " + currencySymbol + totalString
        cell.ppgLabel.text = "PPG: " + currencySymbol + ppgString
        
        
        
        return cell
        
    }
    
    //UITableViewDelegate
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
         /// DELETE FILLUP HERE
            
            // Pop up a dialog to confirm this operation.
            var aAlert = UIAlertController(title: "Question", message: "Do you wish to delete this fillup?\nThis action cannot be undone.", preferredStyle: UIAlertControllerStyle.Alert)
            aAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { Void in
                println("Cancel pressed")
            }))
            
            aAlert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Destructive, handler: { Void in
                let db = DatabaseManager(dbaseFile: "gmt.db")
                if let dti = self.detailItem {
                    var carIdNumber = dti.get(indexPath.row).carId
                    var fillupIdNumber = dti.get(indexPath.row).fillupID
                    var sql:String = "DELETE FROM FILLUP WHERE ID = \(fillupIdNumber) AND CARID = \(carIdNumber)"
                    // If the database update was not successful, then keep the underlying object intact to prevent data corruption
                    if (!db.executeRawStatements(sql)) {
                        println("Error deleting base database object. Keeping local object intact.")
                        return
                    }
                    dti.removeFillupAtIndex(indexPath.row)     // Database update was successful. Delete the underlying object
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)      // and reload the data
                }
            }))
            
            presentViewController(aAlert, animated: true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // This will segue to the AddEditViewController, and fill in the values on the screen with the fillup object being passed to it.
        selectedIndex = indexPath.row                                           // Set the index of the item we are editing so we can pass it on
        performSegueWithIdentifier("presentAddEditWindowForEdit", sender: self)        // Perform the segue to the AddEditViewController
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println(segue.identifier)
        let controller = segue.destinationViewController as AddEditViewController
        if segue.identifier == "presentAddEditWindowForEdit" {
            controller.detailItem = detailItem?.get(selectedIndex)              // Set the detailItem on AddEditViewController to the fillup we want to edit
            controller.editMode = true
            controller.currentCarId = selectedCarId
            controller.currentEditingId = detailItem?.get(selectedIndex).fillupID
            controller.delegate = self
        } else if segue.identifier == "presentAddEditWindow" {
            controller.detailItem = Fillup(carIdFK: selectedCarId!)             // Call convienence initializer to initialize a blank fillup object
            controller.editMode = false
            controller.currentCarId = selectedCarId
            controller.delegate = self
        }
    

        
    }
    
    func calculateGasMileage(listOfFillupMileages: [Int32], listOfFillupAmounts: [Double]) -> String {
        let divider: Int = listOfFillupMileages.count
        var counter: Int = 0
        var fillupCounter: Double = 0.0
        
        
        for index in 1..<listOfFillupMileages.count {
            var toTest = listOfFillupMileages[index] - listOfFillupMileages[index-1]            // Subtract the current and prior fillup mileages
            counter += Int(toTest)
            fillupCounter += listOfFillupAmounts[index-1]
            
        }
        
        var totalMpg = Double(counter)/fillupCounter
        var formatter = String(format: "%.2f", totalMpg)

        return formatter
    }
    
    func didSave(fillup: Fillup) {
        detailItem?.add(fillup)
        fillupList.reloadData()
    }
    
    func didEdit(fillup: Fillup) {
        
        detailItem?.replace(fillup, atIndex: selectedIndex)
        fillupList.reloadData()
    }


}





