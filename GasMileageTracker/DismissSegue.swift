//
//  DismissSegue.swift
//  TaskMe
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Stephen Majors. All rights reserved.
//

import UIKit
@objc(DismissSegue)

class DismissSegue: UIStoryboardSegue {
    override func perform() {
        if let controller = sourceViewController.presentingViewController? {
            controller.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
