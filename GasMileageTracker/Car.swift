//
//  Car.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import Foundation

class Car {
    var carName: String
    var initialOdometer: Int32
    var fstore: FillupStore
    
    init(carName: String, initialOdometer: Int32, fillupStore: FillupStore) {
        self.carName = carName
        self.initialOdometer = initialOdometer
        self.fstore = fillupStore
    }
    
    func getFillupStore() -> FillupStore {
        return fstore
    }
}