//
//  AddEditViewController.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/5/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import UIKit

protocol AddEditViewControllerDelegate {
    func didSave(fillup: Fillup)
    func didEdit(fillup: Fillup)
}

class AddEditViewController: UIViewController, UITextFieldDelegate {
    
    var detailItem: Fillup? {
        didSet {
            // Update the view.
            self.configureView()
        }
        
    }
    
    @IBAction func hiddenButtonPressed(sender: AnyObject) {
        if fuelPrice.text != "" && fuelQuantity.text != "" {        // Check to make sure there is text in the textfields
            var fp = (fuelPrice.text as NSString).doubleValue
            var fa = (fuelQuantity.text as NSString).doubleValue
            var amount = fp*fa
            if amount == 0.00 {                         // Invalid characters were entered, and the double values are zero so just return
                return
            }
            fuelTotalPrice.text = String(format: "%.2f", amount)
        }

    }
    var delegate: AddEditViewControllerDelegate? = nil
    let dbasefile: String = "gmt.db"
    var currentEditingId: Int32?
    var currentCarId: Int32?
    var editMode: Bool?

    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var odometer: UITextField!
    @IBOutlet weak var fuelPrice: UITextField!
    @IBOutlet weak var fuelQuantity: UITextField!
    @IBOutlet weak var fuelTotalPrice: UITextField!
    @IBOutlet weak var filled: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println("Loading Add/Edit screen")
        
        // We are getting here from the edit segue, and there is something in detailItem
        if editMode == true {
            navBar.topItem?.title = "Editing Fillup"
            if let dtitem = detailItem {
                editMode = true
                var odo = detailItem?.fillupOdometer.description                // Get odometer
                var fp = detailItem?.pricePerGallon.description                 // Get fuel price
                var fq = detailItem?.gallonsFilled.description                  // Get filled amount
                var tp = detailItem?.totalSpent.description                     // Get total spent
                currentEditingId = detailItem?.fillupID                         // Set the id of the fillup being edited
                currentCarId = detailItem?.carId                                // Set the id of the car being edited
                
                
                odometer.text = odo                                             // Set all the fields to what is currently stored in the fillup object
                fuelPrice.text = fp
                fuelQuantity.text = fq
                fuelTotalPrice.text = tp
                
                // Set the segmented control based on fillup status
                if detailItem?.filled == true {
                    filled.selectedSegmentIndex = 0
                } else {
                    filled.selectedSegmentIndex = 1
                }
                
            } else {
                navBar.topItem?.title = "Add New Fillup"
                //editMode = false
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView() {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "dismissWithSave") {
            
        } else if (segue.identifier == "dismissWithCancel") {
            
        } else {
            
        }
        
        odometer.text = ""
        fuelPrice.text = ""
        fuelQuantity.text = ""
        fuelTotalPrice.text = ""
        
        // Clear object
        detailItem = nil
        
        
    }
    
    private func checkValidityOfFillupBeforeSaving() -> Bool {
        var odometerCurrent: Int = odometer.text.toInt()!
        var dbase = DatabaseManager(dbaseFile: dbasefile)
        var sqlstmt: String = "SELECT * FROM FILLUP WHERE ID = \(currentEditingId! - 1)"
        let priorFillup = dbase.executeRawQuery(sqlstmt)                                   // Get prior fillup, if it exists
        if (priorFillup.next()) {
            let dict = priorFillup.resultDictionary()                                      // Translate resultset to dictionary
            if !dict.isEmpty {
                for keys in dict {
                    if keys.0 == "FILLUPODOMETER" {
                        var priorOdometer: Int = keys.1 as Int
                        if priorOdometer >= odometerCurrent {                              // the record before cannot be greater than the current record
                            dbase.queryDatabase?.close()
                            return false
                        }
                        
                    }
                }
            }
        }
        
        dbase.queryDatabase?.close()
        sqlstmt = "SELECT * FROM FILLUP WHERE ID = \(currentEditingId! + 1)"                // Get the record after this one
        let postFillup = dbase.executeRawQuery(sqlstmt)
        if postFillup.next() {
            let postDict = postFillup.resultDictionary()
            if !postDict.isEmpty {
                for keys in postDict {
                    if keys.0 == "FILLUPODOMETER" {
                        var postOdometer: Int = keys.1 as Int
                        if postOdometer <= odometerCurrent {
                            // Alert user of errorneous entry and cancel
                            dbase.queryDatabase?.close()
                            return false
                        }
                    }
                }
            }
        }
        
        dbase.queryDatabase?.close()
        
        return true                 // Fillup is in a consistent state
        
    }
    
    private func createSqlQueryParts() -> [String] {
        var returnStatement: [String] = []
        var date: NSDate = NSDate()
        var unixTime = date.timeIntervalSince1970.description
        var unixTimeArray = split(unixTime) { $0 == "." }
        var unix = unixTimeArray[0]
        returnStatement.append(odometer.text)
        returnStatement.append(fuelPrice.text)
        returnStatement.append(fuelTotalPrice.text)
        returnStatement.append(fuelQuantity.text)
        returnStatement.append(unix)
        returnStatement.append(filled.selectedSegmentIndex.description)
        return returnStatement
    }

    @IBAction func saveButtonPressed(sender: AnyObject) {
        //if checkValidityOfFillupBeforeSaving() || true {
        if true {
            var dbase = DatabaseManager(dbaseFile: "gmt.db")
            
            // Fillup is valid
            // Save button pressed, notify delegates to update database references
            
            /// Editing mode
            if editMode == true {
                var sqlPartsEdit = createSqlQueryParts()                    // Break up all the fields into text strings
                if let cid = currentCarId?.description {                    // And update all the parts individually
                    if let fid = currentEditingId {
                        var sqlString: String = "UPDATE FILLUP SET FILLUPODOMETER = \(sqlPartsEdit[0]) WHERE CARID = \(cid) AND ID = \(fid)"
                        if !dbase.executeRawStatements(sqlString) {
                            println("Error updating odometer")
                        }
                        sqlString = "UPDATE FILLUP SET PPG = \(sqlPartsEdit[1]) WHERE CARID = \(cid) AND ID = \(fid)"
                        if !dbase.executeRawStatements(sqlString) {
                            println("Error updating ppg")
                        }
                        sqlString = "UPDATE FILLUP SET TOTALPRICE = \(sqlPartsEdit[2]) WHERE CARID = \(cid) AND ID = \(fid)"
                        if !dbase.executeRawStatements(sqlString) {
                            println("Error updating totalprice")
                        }
                        sqlString = "UPDATE FILLUP SET GALLONSFILLED = \(sqlPartsEdit[3]) WHERE CARID = \(cid) AND ID = \(fid)"
                        if !dbase.executeRawStatements(sqlString) {
                            println("Error updating gallonsfilled")
                        }
                        // Insert date update here. For now, since there is no date entry box, we will not update the date on edit
                        
                        sqlString = "UPDATE FILLUP SET FILLED = \(sqlPartsEdit[5]) WHERE CARID = \(cid) AND ID = \(fid)"
                        if !dbase.executeRawStatements(sqlString) {
                            println("Error updating filled flag")
                        }
                        
                        // Section to edit detail object here
                        var odo = Int32(odometer.text.toInt()!)
                        var ppg = (fuelPrice.text as NSString).doubleValue
                        var fuel = (fuelQuantity.text as NSString).doubleValue
                        var total = (fuelTotalPrice.text as NSString).doubleValue
                        var isFilled = filled.selectedSegmentIndex
                        
                        // Update the detail item
                        detailItem?.fillupOdometer = odo
                        detailItem?.pricePerGallon = ppg
                        detailItem?.gallonsFilled = fuel
                        detailItem?.totalSpent = total
                        detailItem?.filled = isFilled.toBool()!
                    }
                }
                
                // Notify delegate of edit complete with the updated fillup
                delegate?.didEdit(detailItem!)

            } else {
                
                var sqlparts = createSqlQueryParts()
                if let cid = currentCarId?.description {
                    var sql: String = "INSERT INTO FILLUP(FILLUPODOMETER,PPG,TOTALPRICE,GALLONSFILLED,DATEFILLED,FILLED,CARID) VALUES(\(sqlparts[0]), \(sqlparts[1]), \(sqlparts[2]), \(sqlparts[3]), \(sqlparts[4]), \(sqlparts[5]), \(cid))"
                    if !dbase.executeRawStatements(sql) {
                        println("Error saving to database")
                    }
                }

                
                // Section to create new fillup object
                var odo = Int32(odometer.text.toInt()!)
                var ppg = (fuelPrice.text as NSString).doubleValue
                var fuel = (fuelQuantity.text as NSString).doubleValue
                var total = (fuelTotalPrice.text as NSString).doubleValue
                var isFilled = filled.selectedSegmentIndex
                var time = Int32(sqlparts[4].toInt()!)
                var sql: String = "SELECT ID FROM FILLUP WHERE ODOMETER = \(odo.description)"
                var fid: Int = dbase.lastInsertId!
                let returnFillup: Fillup = Fillup(carIDFK: currentCarId!, carFillupOdo: odo, fuelPricePerGallon: ppg, fuelGallonsFilled: fuel, fuelTotalSpent: total, dateOfFillup: time, carFilled: isFilled.toBool()!, cFillupID: Int32(fid))
                delegate?.didSave(returnFillup)
                
                
                
                
            }
            
            performSegueWithIdentifier("dismissWithCancel", sender: self)
            
        } else {
            // Erroneous state of current fillup was detected
        }
        

    }
}
