//
//  FillupStore.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/3/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import Foundation
class FillupStore {
    
    var fillups: [Fillup] = []
    
    func add(fillup: Fillup) {
        fillups.append(fillup)
    }
    
    func replace(fillup: Fillup, atIndex index: Int) {
        fillups[index] = fillup
    }
    
    func get(index: Int) -> Fillup {
        return fillups[index]
    }
    
    func removeFillupAtIndex(index: Int) {
        fillups.removeAtIndex(index)
    }
    
    func count() -> Int {
        return fillups.count
    }
}
