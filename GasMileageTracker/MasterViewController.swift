//
//  MasterViewController.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/1/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import UIKit

var dbaseManager = DatabaseManager?()
let dbaseFile: String = "gmt.db"


class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var carNameTextField: UITextField?
    var carOdometerTextField: UITextField?
    
    //MARK: DEBUG
    let carsStore = CarStore.sharedInstance


    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Initialize the database manager
        
        dbaseManager = DatabaseManager(dbaseFile: dbaseFile)                                            // This will be the initial run of the database
                                                                                                        // Tables will be created from this call if they do not exist
        
        /// Get cars from database
        var sql: String = "SELECT * FROM CAR"
        var resultSet = dbaseManager?.executeRawQuery(sql)
        
        if let rset = resultSet {                                                                          // Unwrap resultset
            while (rset.next()) {                                                                          // Iterate through results
                var carName:String = rset.stringForColumn("CARNAME")
                var initOdometer: Int32 = rset.intForColumn("INITIALODOMETER")
                var carID: Int32 = rset.intForColumn("ID")
                
                let addCar = Car(carName: carName, initialOdometer: initOdometer, fillupStore: FillupStore())
                carsStore.add(addCar)
                var sqlFillup: String = "SELECT * FROM FILLUP WHERE CARID = \(carID.description)"                     // Get fillups
                var fillupSet = dbaseManager?.executeRawQuery(sqlFillup)
                var fsetTofStore: [Fillup]
                if let fset = fillupSet {
                    while (fset.next()) {
                        var odo: Int32 = fset.intForColumn("FILLUPODOMETER")
                        var ppg: Double = fset.doubleForColumn("PPG")
                        var fuel: Double = fset.doubleForColumn("GALLONSFILLED")
                        var total: Double = fset.doubleForColumn("TOTALPRICE")
                        var date: Int32 = fset.intForColumn("DATEFILLED")
                        var filled: Bool = fset.boolForColumn("FILLED")
                        var fid: Int32 = fset.intForColumn("ID")
                        let fillup: Fillup = Fillup(carIDFK: carID, carFillupOdo: odo, fuelPricePerGallon: ppg, fuelGallonsFilled: fuel, fuelTotalSpent: total, dateOfFillup: date, carFilled: filled, cFillupID: fid)
                        addCar.getFillupStore().add(fillup)
                    }
                }
            }
        }
        
        
        println(UIDevice.currentDevice().model)
//        if UIDevice.currentDevice().model.lowercaseString.rangeOfString("ipad") != nil {
//            self.tableView.selectRowAtIndexPath(NSIndexPath(index: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)     // To trigger segue
//        }
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()


        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = controllers[controllers.count-1].topViewController as? DetailViewController
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                
                let toSend = carsStore.getFillupStore(indexPath.row) as FillupStore
                let controller = (segue.destinationViewController as UINavigationController).topViewController as DetailViewController
                controller.detailItem = toSend
                /// SQL to get car id
                
                let dbase = DatabaseManager(dbaseFile: "gmt.db")
                var sql: String = "SELECT ID, INITIALODOMETER FROM CAR WHERE CARNAME = \'\(carsStore.get(indexPath.row).carName)\'"
                var rset = dbase.executeRawQuery(sql)
                
                if (rset.next()) {
                    controller.selectedCarId = rset.intForColumn("ID")
                    controller.initialFillupMileage = rset.intForColumn("INITIALODOMETER")
                } else {
                    controller.selectedCarId = -1                            // Default case of -1. Indicates an error to DetailViewController
                    controller.initialFillupMileage = -1
                }
                
                /// End SQL
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            } else {
                // No row was selected which means either this is the initial setup
                // or we came here from the row delete code
                let splitController = self.splitViewController!.viewControllers
                self.detailViewController = splitController.last? as? DetailViewController
                if let detail = detailViewController {
                    detail.title = "Select a Car or create a new car"
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CarStore.sharedInstance.count()
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        let car = carsStore.get(indexPath.row)
        cell.textLabel?.text = car.carName

        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let dbase = DatabaseManager(dbaseFile: "gmt.db")
            var sql: String = "SELECT ID FROM CAR WHERE CARNAME = \'\(carsStore.get(indexPath.row).carName)\'"              // Get the ID we are supposed to delete
            var rs: FMResultSet = dbase.executeRawQuery(sql)
            var id: Int32?
            if (rs.next()) {
                id = rs.intForColumn("ID")                              // Get the rowID
            } else {
                println("Fatal error determining what id to delete!")
                return
            }
            
            dbase.queryDatabase?.close()
            
            sql = "DELETE FROM CAR WHERE ID = \(id!)"
            if !dbase.executeRawStatements(sql) {
                println("Could not delete from database. Not continuing")
                return
            }
            
            carsStore.removeCarAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            println(carsStore.count())
            if self.splitViewController?.viewControllers.count == 2 {
                self.performSegueWithIdentifier("showDetail", sender: self)
            }
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    @IBAction func addVehicle(sender: AnyObject) {
        
        // Present a view that allows a user to enter a new car
        
        var alert = UIAlertController(title: "Add a Vehicle", message: "Enter Information", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        var okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: { Void in
            
            var nameEmpty = false
            var odoEmpty = false
            var error: String = ""
            
            var name = self.carNameTextField?.text!
            var odometer = self.carOdometerTextField?.text!
            
            if let nm = name {
                if (nm.isEmpty) {
                    error += "You must enter a name.\n"
                }
            }
            
            if let odo = odometer {
                if (odo.isEmpty) {
                    error += "You must enter an initial odometer reading"
                }
            }
            
            if !error.isEmpty {
                var aAlert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.Alert)
                var aAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil)
                aAlert.addAction(aAction)
                self.presentViewController(aAlert, animated: true, completion: nil)
                return
            }
            
            // Get reference to database, and insert new car into database
            let dbase = DatabaseManager(dbaseFile: "gmt.db")
            var sql: String = "INSERT INTO CAR(CARNAME, INITIALODOMETER) VALUES (\'\(name!)\', \(odometer!))"
            
            if !dbase.executeRawStatements(sql) {
                println(dbase.getLastError())
            } else {
                println("Successful")
                // Create new Car object, backup object FillupStore for the car, and add the Car to the static CarStore
                var newCar: Car = Car(carName: name!, initialOdometer: Int32(odometer!.toInt()!), fillupStore: FillupStore())
                self.carsStore.add(newCar)
                self.tableView.reloadData()
            }
            
        })
        
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        alert.addTextFieldWithConfigurationHandler(configCarNameTextHandler)
        alert.addTextFieldWithConfigurationHandler(configCarOdometerTextHandler)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
    private func configCarNameTextHandler(textField: UITextField!) {
        if let tfield = textField {
            tfield.placeholder = "Car Name"
            carNameTextField = tfield
        }
    }
    
    private func configCarOdometerTextHandler(textField: UITextField!) {
        if let tfield = textField {
            tfield.placeholder = "Initial Odometer Value"
            tfield.keyboardType = UIKeyboardType.NumberPad
            carOdometerTextField = tfield
        }
    }
    


}

