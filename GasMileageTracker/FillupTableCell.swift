//
//  FillupTableCell.swift
//  GasMileageTracker
//
//  Created by Stephen Majors on 4/4/15.
//  Copyright (c) 2015 Majors Creations. All rights reserved.
//

import UIKit

class FillupTableCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ppgLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var mpgLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
